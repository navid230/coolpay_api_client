## **Description**
A simple PHP based RESTFUL app which utilizes Laravel framework to present an Interface to users, in order for them to authenticate and use the RESTFUL requests to communicate with the coolpay API and retrieve a list of payments and recipients and also insert payment details and recipient details into the coolpay API.

It utilizes the dingo library and JWTAuth library for authentication purposes.

I decided to build this app in PHP as to make the tasks slightly more challenging. As it has been a few years since I last worked with PHP Laravel framework.

## **Scenario**
Coolpay is a new company that allows to easily send money to friends through their API.

You work for Fakebook, a successful social network. You’ve been tasked to integrate Coolpay inside Fakebook. A/B tests show that users prefer to receive money than pokes!

You can find Coolpay documentation here: http://docs.coolpayapi.apiary.io/

You will write a small app that uses Coolplay API in Ruby. The app should be able do the following:
- Authenticate to Coolpay API
- Add recipients
- Send them money
- Check whether a payment was successful


## **Notes**
* In order to run this app you require the following details

**1. create a DatabaSeeder.php file in the following directory 'app/database/seeds/' and insert the following details appropriately:**

*<?php
class DatabaseSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Eloquent::unguard();

    // $this->call('UserTableSeeder');
    DB::table('users')->delete();
    User::create(array(
        'firstname'     => '[your firstname]',
        'lastname'     => '[your lastname]',
        'email'    => '[your email address]',
        'password' => Hash::make('[the password you will use to authenticate against the REST api]'),
        'confirmation_code' => '12345',
        'coolpay_username' => '[your coolpay username]',
        'coolpay_api_key' => '[your coolpay api key]',
        'confirmed' => true,
    ));
  }
}*

**2. You must have a MySQL database.**

**3. Configure the database.php file in '/app/config/' to hold the appropriate details based on your mysql database credentials. for example: **

'mysql' => array(
'driver'    => 'mysql',
'host'      => 'localhost',
'database'  => '[database name]',
'username'  => '[database username]',
'password'  => '[database password]',
'charset'   => 'utf8',
'collation' => 'utf8_unicode_ci',
'prefix'    => '',
),

**4. You must execute the following command for php artisan to setup the database schema**
     'php artisan migrate'**