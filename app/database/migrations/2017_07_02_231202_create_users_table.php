<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    // Creates the users table
    Schema::create('users', function(Blueprint $table)
    {
      $table->increments('id');

      $table->string('firstname', 32);
      $table->string('lastname', 32);
      $table->string('email', 320)->unique();
      $table->string('password', 64);
      $table->string('confirmation_code');
      $table->string('coolpay_username');
      $table->string('coolpay_api_key');
      $table->boolean('confirmed')->default(false);
      $table->timestamps();
    });

  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('users');
  }

}
