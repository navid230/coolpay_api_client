<?php 

class Payment extends Eloquent {
  public static $rules = array(
    'amount' => 'required',
    'currency' => 'required',
    'recipient_id' => 'required',
    'status' => 'required'
  );
  public static $messages = array(
    'amount.required' => 'Please insert the payment amount.',
    'currency.required' => 'Please insert the currency type.',
    'recipient_id.required' => 'Please insert the recipient ID.',
    'status.required' => 'Please insert the payment status.'
  );

  public static function createPayment($paymentDetails)
  {
    $token = unserialize(Session::get('user_coolpay_t'));
    if($token)
    {
      try
      {
        // check if recipientID was passed
        // var_dump($paymentDetails);
        // die('test');
        if(!empty($paymentDetails->recipientId))
        {
          echo "Authorization: ".$token."\n";
    
          // insert new payment
          $values = '{"payment": {"amount": '.$paymentDetails->amount.', "currency": "'.$paymentDetails->currency.'", "recipient_id": "'.$paymentDetails->recipientId.'", "status": "'.$paymentDetails->status.'"} }';
      
          $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$token.""
          );
          $response = RestClient::post('https://coolpay.herokuapp.com/api/payments', 
            $values, $headers);

          return API::response()->array(['status' => 'success', 'payment' => $response])->statusCode(200);
          
        }else{
          return API::response()->array(['status' => 'failed', 'message' => 'invalid recipient ID'])->statusCode(200);
        }
      } catch (Exception $ex) {
          return API::response()->array(['status' => 'failed', 'message' => 'internal server error'])->statusCode(200);
      }
    }else{
          return API::response()->array(['status' => 'failed', 'message' => 'token expired, login again'])->statusCode(200);
        }
  }

  public static function getPaymentList()
  {
    $token = unserialize(Session::get('user_coolpay_t'));

    if($token){
      try {
        $headers = array(
          "Content-Type: application/json",
          "Authorization: Bearer ".$token.""
        );

        $response = RestClient::get('https://coolpay.herokuapp.com/api/payments', $headers);
        
        $data = json_decode($response->getContent())->payments;
        
        if (!empty($data)) {
          return API::response()->array(['status' => 'success', 'payments' => $data])->statusCode(200);
        }else
        {
          return API::response()->array(['status' => 'failed', 'message' => 'no record found with that name'])->statusCode(200);
        }

      } catch (Exception $ex) {
          return API::response()->array(['status' => 'failed', 'message' => 'internal server error'])->statusCode(200);
      }
    }else{
      return API::response()->array(['status' => 'failed', 'message' => 'token expired, login again'])->statusCode(200);
    }
  }
}

?>