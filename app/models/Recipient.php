<?php
 
class Recipient extends Eloquent {

  public static $rules = array('name' => 'required');
  public static $messages = array('name.required' => 'Please insert the recipient name.');

  public static function createRecipient($recipientName)
  {
    $token = unserialize(Session::get('user_coolpay_t'));

    if($token){
      try {
        $headers = array(
          "Content-Type: application/json",
          "Authorization: Bearer ".$token.""
        );

        $response = RestClient::get('https://coolpay.herokuapp.com/api/recipients?name='.urlencode($recipientName).'', 
          $headers);
               
        $data = json_decode($response->getContent())->recipients;

        if (!empty($data)) {
          return API::response()->array(['status' => 'failed', 'message' => 'record already exists'])->statusCode(200);
        }

        $values = '{"recipient": {"name": "'.$recipientName.'"} }';

        $headers = array(
          "Content-Type: application/json",
          "Authorization: Bearer ".$token.""
        );
        $response = RestClient::post('https://coolpay.herokuapp.com/api/recipients', 
          $values, $headers);
        return API::response()->array(['status' => 'success', 'recipient' => $response])->statusCode(200);
      } catch (Exception $ex) {
          return API::response()->array(['status' => 'failed', 'message' => 'internal server error'])->statusCode(200);
      }
    }else{
      return API::response()->array(['status' => 'failed', 'message' => 'token expired, login again'])->statusCode(200);
    }
  }

  public static function getRecipientList($name)
  {
    $token = unserialize(Session::get('user_coolpay_t'));

    if($token){
      try {
        $headers = array(
          "Content-Type: application/json",
          "Authorization: Bearer ".$token.""
        );
        if(!empty($name))
        {
          $response = RestClient::get('https://coolpay.herokuapp.com/api/recipients?name='.urlencode($name).'', $headers);
        }else
        {
          $response = RestClient::get('https://coolpay.herokuapp.com/api/recipients', $headers);
        }
        $data = json_decode($response->getContent())->recipients;
        
        if (!empty($data)) {
          return API::response()->array(['status' => 'success', 'recipients' => $data])->statusCode(200);
        }else
        {
          return API::response()->array(['status' => 'failed', 'message' => 'no record found with that name'])->statusCode(200);
        }

      } catch (Exception $ex) {
          return API::response()->array(['status' => 'failed', 'message' => 'internal server error'])->statusCode(200);
      }
    }else{
      return API::response()->array(['status' => 'failed', 'message' => 'token expired, login again'])->statusCode(200);
    }
  }
}

?>