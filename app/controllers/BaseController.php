<?php

class BaseController extends Controller {

  /**
   * Setup the layout used by the controller.
   *
   * @return void
   */
  protected function setupLayout()
  {
    if ( ! is_null($this->layout))
    {
      $this->layout = View::make($this->layout);
    }
  }


  protected function coolpayTokenAuth()
  {
    if (! Session::has('user_coolpay_t'))
    {
      $user = API::user();

      $username = $user->coolpay_username;
      $api_key = $user->coolpay_api_key;
      // $values = '{ "username": "NaveedA", "apikey": "944E0B400504AE67" }';

      $values = '{"username": "'.$username.'", "apikey": "'.$api_key.'"}';

      $headers = array('Content-Type: application/json');

      try {
        $response = RestClient::post('https://coolpay.herokuapp.com/api/login', 
          $values, $headers);
        // echo "HTTP Status Code: " . $response->getStatusCode();
        // echo "HTTP Status Text: " . $response->getStatusText();
        // echo "HTTP content: " . $response->getContent();

        $token = json_decode($response->getContent());
        Session::put('user_coolpay_t', serialize($token->{'token'}));
        
      } catch (Exception $ex) {
          print "Error: " . $ex->getMessage(); // Error: COULDNT_RESOLVE_HOST
      }
    }
  }
}
