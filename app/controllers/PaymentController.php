<?php

class PaymentController extends \BaseController {
  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function index()
  {
    parent::coolpayTokenAuth();
    return Payment::getPaymentList();
  }

  public function store()
  {

    parent::coolpayTokenAuth();
    $validation = Validator::make(Request::all(), Payment::$rules, 
      Payment::$messages);

    if($validation->fails()){

      return API::response()->array(['status' => 'failed', 'message' => $validation->errors()])->statusCode(200);
    }
    $paymentDetails = new Payment;
    $paymentDetails->amount = Request::get('amount');
    $paymentDetails->currency = Request::get('currency');
    $paymentDetails->recipientId = Request::get('recipient_id');
    $paymentDetails->status = Request::get('status');

    return Payment::createPayment($paymentDetails);
  }

}
?>