<?php  
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends \BaseController{

  public function authenticate() {
    $credentials = Input::only('email', 'password');
    try {

      if (! $token = JWTAuth::attempt($credentials)) {
        return API::response()->array(['error' => 'invalid_credentials'])->statusCode(401);
      }
    } catch (JWTException $e) {

      return API::response()->array(['error' => 'could_not_create_token'])->statusCode(500);
    }

    return compact('token');
  }

  public function validateToken()
  {
    return API::response()->array(['status' => 'success'])->statusCode(200);
  }
}
?> 