<?php

class RecipientController extends \BaseController {
  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */

  public function index()
  {
    parent::coolpayTokenAuth();
    return Recipient::getRecipientList(null);
  }

  public function store()
  {
    parent::coolpayTokenAuth();
    $validation = Validator::make(Request::all(), Recipient::$rules, 
      Recipient::$messages);

    if($validation->fails()){

      return API::response()->array(['status' => 'failed', 'message' => $validation->errors()])->statusCode(200);
    }

    $recipientName = Request::get('name');

    return Recipient::createRecipient($recipientName);

  }
  public function search()
  {
    parent::coolpayTokenAuth();
    $validation = Validator::make(Request::all(), Recipient::$rules, 
      Recipient::$messages);

    if($validation->fails()){

      return API::response()->array(['status' => 'failed', 'message' => $validation->errors()])->statusCode(200);
    }

    $recipientName = Request::get('name');

    return Recipient::getRecipientList($recipientName);
  }
}