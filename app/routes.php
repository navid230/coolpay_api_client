<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::api('v1', function () {  
	
	Route::post('login', [  
    	'uses'      => 'AuthenticateController@authenticate',
		'as'        => 'api.login'
	]);
	Route::get('validate_token',  array(
    	'protected' => true,
		'as'        =>  'api.validate_token',
		'uses'        => 'AuthenticateController@validateToken'
	));

	Route::get('recipients', [  
    	'uses'      => 'RecipientController@index',
		'as'        => 'RecipientController.index',
		'protected' => true
	]);
	Route::post('recipient', [  
		'uses'      => 'RecipientController@store',
		'as'        => 'RecipientController.store',
		'protected' => true
	]);
	Route::get('recipient', [  
    	'uses'      => 'RecipientController@search',
		'as'        => 'RecipientController.search',
		'protected' => true
	]);


	Route::get('payments', [  
    	'uses'      => 'PaymentController@index',
		'as'        => 'PaymentController.index',
		'protected' => true
	]);
	Route::post('payment', [  
		'uses'      => 'PaymentController@store',
		'as'        => 'PaymentController.store',
		'protected' => true
	]);

});